# in .zshrc include:
sync_progress() {
    sync & watch -n1 'grep -E "(Dirty|Write)" /proc/meminfo; echo; ls /sys/block/ | while read device; do awk "{ print \"$device: \"  \$9 }" "/sys/block/$device/stat"; done; echo; echo -n Count of active sync processes:\ ; pgrep -c sync'
}
